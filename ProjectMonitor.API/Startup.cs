﻿using Microsoft.Owin;
using Owin;

namespace ProjectMonitor.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseWebApi(WebApiConfig.Register());

        }
    }
}