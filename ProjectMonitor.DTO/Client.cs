﻿using System;
using System.Collections.Generic;

namespace ProjectMonitor.DTO
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Project> Projects { get;  set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
