﻿using System;
using System.Collections.Generic;

namespace ProjectMonitor.DTO
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Client Client { get; set; }
        public int ClientId { get; set; }
        public ICollection<WorkTime> TimeWorked { get; set; }
        public ICollection<ProjectDescription> ProjectDescriptions { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
