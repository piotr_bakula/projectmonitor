﻿using System;

namespace ProjectMonitor.DTO
{
    public class ProjectDescription
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
