﻿using System;
using System.Collections.Generic;

namespace ProjectMonitor.DTO
{
    public class User

    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public ICollection<WorkTime> TimeWorked { get; set; }
        public ICollection<ProjectDescription> ProjectDescriptions { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
