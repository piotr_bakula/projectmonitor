﻿using ProjectMonitor.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectMonitor.Repository.Entities
{
    public class Client : IModificationHistory
    {
        public Client()
        {
            Projects = new HashSet<Project>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "Text")]
        public string Description { get; set; }
        public ICollection<Project> Projects { get;  set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
