﻿using ProjectMonitor.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectMonitor.Repository.Entities
{
    public class Project : IModificationHistory
    {
        public Project()
        {
            Users = new HashSet<User>();
            TimeWorked = new HashSet<WorkTime>();
            ProjectDescriptions= new HashSet<ProjectDescription>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName ="Text")]
        public string Description { get; set; }
        public Client Client { get; set; }
        public int ClientId { get; set; }
        public ICollection<WorkTime> TimeWorked { get; set; }
        public ICollection<ProjectDescription> ProjectDescriptions { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
