﻿using ProjectMonitor.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMonitor.Repository.Entities
{
    public class ProjectDescription : IModificationHistory
    {
        public int Id { get; set; }
        [Column(TypeName ="Text")]
        public string Description { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
