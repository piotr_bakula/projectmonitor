﻿using ProjectMonitor.Repository.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectMonitor.Repository.Entities
{
    public class User : IModificationHistory

    {
        public User()
        {
            Projects = new HashSet<Project>();
            TimeWorked = new HashSet<WorkTime>();
            ProjectDescriptions = new HashSet<ProjectDescription>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public ICollection<WorkTime> TimeWorked { get; set; }
        public ICollection<ProjectDescription> ProjectDescriptions { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
