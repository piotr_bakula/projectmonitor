﻿using ProjectMonitor.Repository.Interfaces;
using System;

namespace ProjectMonitor.Repository.Entities
{
    public class WorkTime : IModificationHistory
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
        public int MinutesWorked { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
