﻿using System;

namespace ProjectMonitor.Repository.Interfaces
{
    public interface IModificationHistory
    {
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }
    }
}
