﻿using ProjectMonitor.Repository.Entities;
using System.Data.Entity;

namespace ProjectMonitor.Repository
{
    public class ProjectMonitorContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<WorkTime> WorkTimes { get; set; }
        public DbSet<ProjectDescription> ProjectDescriptions { get; set; }
    }
}
